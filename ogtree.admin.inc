<?php

/**
 * Administrative settings form.
 */
function ogtree_configure($form, &$form_state) {
  $form = array();
  $form['ogtree_nid'] = array(
    '#type' => 'select',
    '#title' => t('Top-level OG node'),
    '#required' => TRUE,
    '#options' => ogtree_groups(),
    '#default_value' => variable_get('ogtree_nid', NULL),
    '#description' => t('The default top-level group of the OG Tree.'),
  );
  $form['ogtree_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Group types'),
    '#required' => TRUE,
    '#options' => ogtree_types(),
    '#default_value' => variable_get('ogtree_types', NULL),
    '#description' => t('Select the content type(s) which will be shown on the OG tree.'),
  );
  $form['ogtree_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Leadership roles'),
    '#required' => TRUE,
    '#options' => og_get_global_roles(),
    '#default_value' => variable_get('ogtree_roles', array()),
    '#description' => t('Select the group membership role(s) which will be shown on the OG tree.'),
  );
  $form['ogtree_depth'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum tree depth'),
    '#required' => TRUE,
    '#default_value' => variable_get('ogtree_depth', 0),
    '#description' => t('The root node is depth one; its children are depth two, and so on. Set to zero or negative for no limit.'),
    '#validate' => '_element_validate_integer',
  );
  $form['ogtree_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom css path'),
    '#default_value' => variable_get('ogtree_css', ''),
    '#description' => t('Specify a path to a custom css file, or leave blank to use the module-provided %css file.', array('%css' => 'ogtree.css')),
    '#validate' => 'ogtree_validate_css_filename',
  );
  return system_settings_form($form);
}
