<div class="ogtree ogtree-<?php print $nid; ?>">
  <div class="ogtree-title">
    <?php print $title; ?>
  </div>
  <div class="ogtree-leaders">
    <?php print $leaders; ?>
  </div>
  <?php if ($body): ?>
  <div class="ogtree-body">
   <div class="ogtree-body-label">&nbsp;</div>
   <div class="ogtree-body-value"><?php print $body; ?></div>
  </div>
  <?php endif; ?>
  <div class="ogtree-children">
    <?php print $children; ?>
  </div>
</div>
